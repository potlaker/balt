const express = require('express');
const bodyParser = require('body-parser')
const cors = require('cors');
const parseRouter = require('./routes/parseRouter');
const app = express();

app.use(cors());
app.use(bodyParser.json());
app.use(express.static(__dirname));

app.use('/api/parse', parseRouter);

const server =  app.listen('3001', ()=>{
    console.log("server is run...")
});

global.io = require('./socket.js')(server);

io.on('connection', socket => {
    console.log('socket connections!!!');
    setTimeout(function() {
        //Sending an object when emmiting an event
        socket.emit('customEmit', { description: 'Это работает!!!'});
     }, 1000);
  
     socket.on('disconnect', function () {
        console.log('A user disconnected');
     });
});

