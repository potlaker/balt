const io = require('socket.io');

let instanceIO;
module.exports = function(server){
    // if(!instanceIO){
        instanceIO = io(server, {
            serveClient: false,
            allowEIO3: true,
            cors: {
                origin: true,
                credentials: true,
              }
        });
    // }
    return instanceIO;
}

