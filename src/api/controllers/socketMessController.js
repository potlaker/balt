
/**
 * Отправляет socket сообщение об ошибке
 * @param {string} title - Заголовок сообщения
 * @param {string} text - Текст сообщения 
 */
exports.errMess = async function({title, text}){
    io.emit('message', {
        title,
        text,
        type: 'error',
    });
};
/**
 * Отправляет socket сообщение информирования
 * @param {string} title - Заголовок сообщения
 * @param {string} text - Текст сообщения 
 */
exports.infMess = function({title, text}){
    console.log('infoooo');
    io.emit('message', {
        title,
        text,
        type: 'info',
    });
};
/**
 * Отправляет socket сообщение предупреждения
 * @param {string} title - Заголовок сообщения
 * @param {string} text - Текст сообщения 
 */
exports.warMess = async function({title, text}){
    io.emit('message', {
        title,
        text,
        type: 'warning',
    });
}
/**
 * Отправляет socket сообщение успеха
 * @param {string} title - Заголовок сообщения
 * @param {string} text - Текст сообщения 
 */
exports.sucMess = async function({title, text}){
    io.emit('message', {
        title,
        text,
        type: 'success',
    });
}