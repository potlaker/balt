const fs = require('fs');
const path = require('path');
const { writeFile } = require('fs/promises');
const { v4: uuidv4 } = require('uuid');

/**
 * пишет данные в json файл
 * @param {array} arr массив объектов
 */
exports.writeJson = async function(arr){
	try{
		let jsonArr = JSON.stringify(arr);
		let pathFile = path.resolve(__dirname, '../', 'files/', `${uuidv4()}.json`);
		await writeFile(pathFile, jsonArr);
		messSocket.sucMess({
			title: 'Файл успешно создан',
			text: 'Все данные записаны в json файл',
			});
	}catch(err){
		messSocket.errMess({
			title: 'Ошибка записи файла',
			text: 'Не удалось записать данные в json файл',
			});
		throw err;    
	}
}