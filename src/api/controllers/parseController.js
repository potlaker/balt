const browserControl = require('./browserController.js');
const parseAd = require('./parseAdController.js');
const file = require('./fileController.js');
const messSocket = require('./socketMessController.js');


/**
 * Исполнитель парсинга
 * @param {object} request - объект запроса
 * @param {object} responce - объект ответа
 * @returns {undefined}
 */
exports.startParse = async function(request, responce){
    let browser;
    try{ 
        browser = await browserControl.startBrowser(responce);
        let resultArr = [];
        let urlPage = request.body.url;
        let maxPage = request.body.maxPage;
        let adListPage = await browserControl.startPage(browser, urlPage, 1);
        
        let countPage = await getCountPage(adListPage, maxPage);

        for(let i = 0; i < countPage; ++i){
            let adLinkList = await getListAds(adListPage);
            if(adLinkList){
               let avoidId = await getIdsAvoid(adListPage)
                await getDataAds(adLinkList, avoidId, browser, resultArr);
                let buttomNextPage = await getNextPage(adListPage);
                if(buttomNextPage){
                    await buttomNextPage.click();
                }else{
                    break;
                }
            }else{
                break;
            }
        }
        if(resultArr.length){
            await file.writeJson(resultArr);
        }
        messSocket.sucMess({
            title: 'Завершено успешно',
            text: 'Парсинг объявлений успешено завершен.',
        });
    }catch(err){
        console.dir(err);
        messSocket.warMess({
            title: 'Рабочая область закрыта',
            text: 'Ошибка в работе. Попробуйте запустить процесс снова',
        });
    }finally{
        browserControl.endBrowser(browser);
    }
}
/**
 * Проверяет есть ли на страницы объявления и возвращает массив элементов списока
 * @param {object} page - объект страницы
 * @returns {(array|boolean)} Возвращет массив элементов списка объявлений или false
 */
async function getListAds(page){
    try{
        let wrapList = await page.waitForSelector('div[data-marker="catalog-serp"]');
        let adLinkList = await wrapList.$$('div[data-marker="item"] a[data-marker="item-title"] ');
        return adLinkList;
    }catch(err){
        console.dir(err);
        messSocket.infMess({
            title: 'Нет объявлений',
            text: 'На странице не найдено, ни одного объявления.',
        });
        return false;
    }
}
/**
 * Проверяет есть ли на странице объявления с раздела "еще в области"
 * @param {object} page - объект страницы
 * @returns {(array|boolean)} Возвращет массив элементов списка объявлений или false
 */
 async function getIdsAvoid(page){
    try{
        let wrapList = await page.waitForSelector('div[data-marker="catalog-serp"]');
        let avoidList = await wrapList.$$('div[data-marker="witcher/block"] div[data-marker="item"]');
        let avoidIdArr = [];
        if(avoidList.length){
            for(elem of avoidList){
                avoidIdArr.push( await elem.evaluate(node => node.dataset.itemId) );
            }
        }
        return avoidIdArr;
    }catch(err){
        console.dir(err);
        return false;
    }
}
/**
 * 
 * @param {array} adLinkList - массив объявлений (ElementHandle) 
 * @param {array} avoidId - массив id ненужных объявлений
 * @param {object} browser - объект браузера
 * @param {array} listResult - массив для записи результатов
 * @returns {undefined}
 */
async function getDataAds(adLinkList, avoidId, browser, listResult){
    let pageAd;
    try{     
        for(adItem of adLinkList){
            let url = await adItem.evaluate(node => node.href );
            if(avoidId && avoidId.find(id => url.includes(id))){
                continue;
            }
            pageAd = await browserControl.startPage(browser, url, 1, true);
            result = await parseAd.runParse(pageAd, 1);
            await pageAd.close();
            if(!result){
                continue;
            }
            result.url = url;
            console.dir(result);
            listResult.push(result)
        }
    }catch(err){
        console.dir(err);
        await pageAd.close();
        throw err;
    }
}
/**
 * Возвращает количество страниц с объявлениями
 * @param {object} page - объект страницы
 * @param {string} maxPage - число страниц для парсинга
 * @returns {string} количество страниц
 */
async function getCountPage(page, maxPage){
    try{
        let wrapPag = await page.waitForSelector('div[data-marker="pagination-button"]');
        let spanList = await wrapPag.$$('span[data-marker^="page"]');
        let counterMax = await spanList[spanList.length - 1].evaluate(node => node.innerText);

        if(maxPage > counterMax){
           return counterMax;
        }
        return maxPage;
    }catch{
        return 1;
    }
}
/**
 * Возвращает активную кнопку следующей страницы.
 * @param {object} page - объект страницы
 * @returns {(ElementHandle|boolean)} возвращает объект активной кнопки или false
 */
async function getNextPage(page){
    try{
        let button = await page.waitForSelector('span[data-marker="pagination-button/next"]');
        let active = await button.evaluate(node => {
            for(className of node.classList){
                if(className.includes('pagination-item_readonly')){
                    return 'no';
                }
            }
            return 'yes';
        });
        if(active === 'yes'){
            return button;
        }
        return false;
    }catch(err){
        console.dir(err);
        return false;
    }
}