const puppeteer = require('puppeteer');
const messSocket = require('./socketMessController.js');

/**
 * Запускает браузер и возвращает
 * @param {object} responce - объект ответа сервера
 * @returns {jbject} объект браузера
 */
exports.startBrowser = async function startBrowser(responce){
    let browser;
    try{
        browser = await puppeteer.launch({
            // headless: true,
            headless: false,
            'ignoreHTTPSErrors': true,
            defaultViewport : {
                width: 1366,
                height: 768,
            },
            timeout: 40000,
        });
        responce.status(200).send({
            status: true,
            title: 'Рабочая область запущена',
            text: `Подготовка к парсингу`,
            type: 'success',
        });
        return browser;
    }catch(err){
        messSocket.errMess({
            title: 'Не удалось запустить браузер',
            text: `${err.name}   ${err.message}`,
        })
        throw err;
    }

}
/**
 * Закрывает открытый браузер
 * @param {object} browser - объект браузера
 */
exports.endBrowser = async function(browser){
    await browser.close();
}
/**
 * Запускает страницу с переданным адресом
 * @param {object} browser - объект браузера
 * @param {string} url - адрес страницы
 * @param {number} counterFailStart - количество попыток запуска страницы
 * @param {boolean} pageAd - активатор создания страницы с эмуляцией устройства
 * @param {number} timeout - количество миллисекунд до перезапуска функции
 * @returns {object} page - объект запущенной страницы
 */
 exports.startPage = async function startPage(browser, url, counterFailStart, pageAd = false, timeout = 17000){
    let page;
    try{
        page = await browser.newPage();
        if(pageAd){
            let  iPhone = puppeteer.devices['iPhone XR'];
            await page.emulate(iPhone);
        }
        await page.goto(url, {
            waitUntil: 'networkidle0',
            timeout,
        });

        return page;
    }catch(err){
        if(counterFailStart > 5){
            messSocket.errMess({
                title: 'Ошибка открытия страницы',
                text: `Более 5 раз попыток запуска страницы: ${url} .`,
            })
            throw err;
        }
        await page.close();
        return await startPage(browser, url, ++counterFailStart, pageAd, timeout + 2000);
    }
}