/**
 * Парсит страницу объявления.
 * @param {elementHandle} page - объект страницы
 * @param {number} tryPage - количество попыток парсинга одной страницы
 * @returns {(object|boolean)} Объект с данными, в случае удачи, иначе false
 */
exports.runParse = async function runParse(page, tryPage){
    
    if(tryPage > 3 || !await checkExistsPhone(page) ){
        return false;
    }
    objResult = {};
    let listFunc = [
        {
            func: getTitle,
            failStatus: false,
            nameStr: 'title',
        },
        {
            func: getDescription,
            failStatus: false,
            nameStr: 'description',
        },
        {
            func: getPrice,
            failStatus: false,
            nameStr: 'price',
        },
        {
            func: getAuthor,
            failStatus: false,
            nameStr: 'author',
        },
        {
            func: getDateISO,
            failStatus: false,
            nameStr: 'date',
        },
        {
            func: getPhone,
            failStatus: false,
            nameStr: 'phone',
        },
    ];
    delay(randomInteger(1003, 5550));

    for(obj of listFunc){
        let result = await obj.func(page, obj.failStatus);
        console.dir(obj.func);
        if(result !== false){
            objResult[obj.nameStr] = result;
        }else if(result === null){
            return false;
        }else{
            obj.failStatus = true;
            await page.reload({waitUntil: 'networkidle2'});
            return await runParse(page, ++tryPage);
        }
    }

    delay(randomInteger(2903, 8550));
    return objResult;
}

/**
 * возвращает заголовок объявления
 * @param {elementHandle} page - объект страницы
 * @param {boolean} failStatus - активатор выброса ошибка
 * @returns {(string|boolean)} Строка в случае удачи, иначе false
 */
async function getTitle(page, failStatus){
    try{
         let h1Elem = await  page.waitForSelector('h1[data-marker="item-description/title"]');
         let title = await h1Elem.evaluate(node => node.innerText);
         return title;
    }catch(err){
        console.dir(err);
        if(failStatus){
            messSocket.errMess({
                title: 'Ошибка парсинга заголовка',
                text: 'Не удалось найти элемент "h1[data-marker="item-description/title"]"',
            });
            throw err;
        }
        return false;
    }
}
/**
 * Возвращает описание объявления
 * @param {elementHandle} page - объект страницы
 * @param {boolean} failStatus - активатор выброса ошибка 
 * @returns {(string|boolean)} Строка в случае удачи, иначе false
 */
async function getDescription(page, failStatus){
    try{
        let descr = await page.$eval('div[data-marker="item-description/text"]', node => node.innerText);
        return descr;
    }catch(err){
        if(failStatus){
            messSocket.errMess({
                title: 'Ошибка парсинга описания',
                text: 'Не удалось найти элемент "div[data-marker="item-description/text"]"',
            });
            throw err;
        }
        console.dir(err);
        return false;
    }
}
/**
 * Возвращает цену товара
 * @param {elementHandle} page - объект страницы
 * @param {boolean} failStatus - активатор выброса ошибка 
 * @returns {(number|boolean)} Число(0 - цена не указана) в случае удачи, иначе false
 */
 async function getPrice(page, failStatus){
    try{
        let price = await page.$eval('span[data-marker="item-description/price"]', node => {
            let matchPrice = node.innerText.replace(/\D/g, "");
            if(!matchPrice.length){
                return 0; 
            }
            return Number(matchPrice);
        });
        return price;
    }catch(err){
        if(failStatus){
            messSocket.errMess({
                title: 'Ошибка парсинга цены',
                text: 'Не удалось найти элемент "span[data-marker="item-description/price"]"',
            });
            throw err;
        }
        console.dir(err);
        return false;
    }
}
/**
 * Возвращает имя автора объявления
 * @param {elementHandle} page - объект страницы
 * @param {boolean} failStatus - активатор выброса ошибка 
 * @returns {(string|boolean)} Строка в случае удачи, иначе false
 */
 async function getAuthor(page, failStatus){
    try{
        let author = await page.$eval('span[data-marker="seller-info/name"]', node => node.innerText);
        return author;
    }catch(err){
        if(failStatus){
            messSocket.errMess({
                title: 'Ошибка парсинга имени автора',
                text: 'Не удалось найти элемент "span[data-marker="seller-info/name"]"',
            });
            throw err;
        }
        console.dir(err);
        return false;
    }
}
/**
 * Возвращает дату объявления в формате iso-8601
 * @param {elementHandle} page - объект страницы
 * @param {boolean} failStatus - активатор выброса ошибка 
 * @returns {(string|boolean)} Строка даты в случае удачи, иначе false
 */
 async function getDateISO(page, failStatus){
    try{
        let date = await page.$eval('div[data-marker="item-stats/timestamp"] span', node => node.innerText);
        return parseDate(date);
    }catch(err){
        if(failStatus){
            messSocket.errMess({
                title: 'Ошибка парсинга даты',
                text: 'Не удалось найти элемент "div[data-marker="item-stats/timestamp"] span"',
            });
            throw err;
        }
        console.dir(err);
        return false;
    }
}
/**
 * Возвращает номер телефона объявления
 * @param {elementHandle} page - объект страницы
 * @param {boolean} failStatus - активатор выброса ошибка 
 * @returns {(string|boolean|null)} Строка телефона в случае удачи, иначе false или null(нет номера)
 */
 async function getPhone(page, failStatus){
    try{
        let activatorPhone = await checkExistsPhone(page);
        await delay(randomInteger(1903, 13850));
        await activatorPhone.click();
        if(await checkLogin(page)){
            console.log('просит логиниться');
            await delay(randomInteger(1003, 2950));
        //    здесь можно расширить логику добавив объявление в массив переобхода с использование прокси и другого устройста
           return null;
        }
        let phoneElem = await page.waitForSelector('div[data-marker="phone-popup"] span[data-marker="phone-popup/phone-number"]');
        let phone = await phoneElem.evaluate(node => node.innerText.replace(/\D/g, ""));
        return phone;
    }catch(err){
        if(failStatus){
            messSocket.errMess({
                title: 'Ошибка парсинга телефона',
                text: 'Не удалось найти элемент "div[data-marker="phone-popup"] span[data-marker="phone-popup/phone-number"]"',
            });
            throw err;
        }
        console.dir(err);
        return false;
    }
}
/**
 * Проверяет есть ли телефон на странице
 * @param {elementHandle} page  - объект страницы
 * @returns {elementHandle} объект кнопки актиации показа телефона
 */
async function checkExistsPhone(page){
    try{
        let activatorPhone = await page.$('div[data-marker="item-contact-bar/contacts-in-body"] a[data-marker="item-contact-bar/call"]');
        return activatorPhone;
    }catch(err){
        return false;
    }
}
/**
 * Вспомогательная функция. Проверяет запрос залогиниться
 * @param {elementHandle} page - объект страницы
 * @returns {boolean}
 */
async function checkLogin(page){
    try{
        await page.waitForSelector('div[data-marker="login-button"]', {timeout: 4000})
        return true;
    }catch{
        return false;
    }
}
/**
 * Вспомогательная функция. Возвращает число в диапазоне min - max
 * @param {number} min - минимальное число
 * @param {number} max - максимальное число
 * @returns {number} число в диапазоне min - max
 */
function randomInteger(min, max) {
    let rand = min - 0.5 + Math.random() * (max - min + 1);
    return Math.round(rand);
}
/**
 * Вспомагательная функция. Парсинга даты из строки и преобразования в формат iso-8601
 * @param {string} dateStr - Строка даты
 * @returns {string} Преобразованная строка даты
 */
function parseDate(dateStr){
    dateStr = dateStr.trim().toLowerCase();
    let objMask = {
        today: new Date(),
        yesterday: new Date(Date.now() - 24 * 3600 * 1000),
        'января': () => {let date = new Date(); date.setMonth(0); return date},
        'февраля': () => {let date = new Date(); date.setMonth(1); return date},
        'марта': () => {let date = new Date(); date.setMonth(2); return date},
        'апреля': () => {let date = new Date(); date.setMonth(3); return date},
        'мая': () => {let date = new Date(); date.setMonth(4); return date},
        'июня': () => {let date = new Date(); date.setMonth(5); return date},
        'июля': () => {let date = new Date(); date.setMonth(6); return date},
        'августа': () => {let date = new Date(); date.setMonth(7); return date},
        'сентября': () => {let date = new Date(); date.setMonth(8); return date},
        'октября': () => {let date = new Date(); date.setMonth(9); return date},
        'ноября': () => {let date = new Date(); date.setMonth(10); return date},
        'декабря': () => {let date = new Date(); date.setMonth(11); return date},
    };
    let timeMatch =  dateStr.match(/(\d{1,2}):(\d{2})/);
    if(!timeMatch.length || timeMatch.length < 3){
        throw new Error(`Не удается распарсить дату: ${dateStr}`);
    }
    let time;
    if(dateStr.includes('сегодня')){
        time = objMask.today;
    }else if(dateStr.includes('вчера')){
        time = objMask.yesterday;
    }else{
        let monthMatch = dateStr.match(/[^\d\w\s\,\:]{3,}/);
        if(!monthMatch.length){
            throw new Error(`Не удается распарсить дату: ${dateStr}`);
        }
        if(monthMatch[0] in objMask){
            time = objMask[monthMatch[0]]();
        }else{
            throw new Error(`Не удается распарсить дату: ${dateStr}`);
        }
        time.setDate(parseInt(dateStr));
    }

    time.setHours(timeMatch[1], timeMatch[2]);
    return time.toISOString();
}
/**
 * Вспомагательная функция. Задержка delay миллисекунд 
 * @param {string} delay - Количество миллисекунд
 * @returns {Promise} Успешно завершенный промис
 */
async function delay(delay){
    return new Promise((resolve, reject)=>{
        setTimeout(() => resolve(true), delay);
    });
}
/**
 * Дожидается загрузки страницы и возвращает исполнившийся промис
 * @param {object} page - объект страницы
 * @returns 
 */
async function delayLoad(page){
    return new Promise((resolve, reject)=>{
        page.on('load', ()=> resolve(true));
    });
}