const express = require("express");
const controller = require("../controllers/parseController");
const parseRouter = express.Router();

parseRouter.post('/start', controller.startParse);
// fileRouter.get('/mail', controller.getMailFile);

module.exports = parseRouter;