import { createStore } from 'vuex'
import parse from './parse.js';

export default createStore({
  modules: {
    parse,
  }
})
