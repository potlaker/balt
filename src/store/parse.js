
export default {
    namespaced: true,
    state: {
        messages: [],
    },
    getters: {
        getMessAll(state){
            return state.messages;
        },
    }, 
    mutations: {
        setMess(state, mess){
            state.messages.push(mess);
        },
    },
    actions: {
        SOCKET_message({ commit }, data){
            // получает сообщения процесса парсинга через io
            commit('setMess', data);
        },
    }
}