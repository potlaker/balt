# balt
```
POST http://localhost:3001/api/parse/start
```
```
params json: "{ url: (string), maxPage: (string)}"
```
```
url: авито ссылка
```
```
maxPage: количество страниц обхода
```
## Project setup
```
npm install
```

### Compiles and hot-reloads for development
```
npm run serve (/src)
```
### Start server
```
node app.js (/src/api)
```
### Compiles and minifies for production
```
npm run build
```

### Customize configuration
See [Configuration Reference](https://cli.vuejs.org/config/).
